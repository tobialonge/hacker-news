'use strict';

// Declare app level module which depends on views, and components
angular.module('hackerNews', [
  'ui.router', 'ui.bootstrap', 'ngResource'
])
.config(['$stateProvider', '$urlRouterProvider',  function ($stateProvider, $urlRouterProvider) {
  
  // For unmatched routes
  $urlRouterProvider.otherwise('/home');

  // Application routes
  $stateProvider
      .state('home', {
          url: '/home',
          templateUrl: 'components/templates/home.html',
           controller: 'homeCtrl'
      })
}]);
