angular.module('hackerNews')
    .controller('homeCtrl', ['$scope', '$log', '$rootScope', 'homeService', '$window', homeCtrl]);

function homeCtrl($scope, $log, $rootScope, homeService, $window) {
    $scope.isLoading = false;
    $scope.noImage = '../../images/image.jpg';
    homeService.newsInfo(function (data) {
        $log.debug('data', data);
        $scope.newsInfo = data.posts;
        $log.debug('$scope.newsInfo ', $scope.newsInfo);
        if ($scope.newsInfo) {
            $log.debug('$scope.newsInfo.length', $scope.newsInfo.length);
            if ($scope.newsInfo.length === 0) {
                $scope.empty = true;
                $log.debug('empty ', $scope.empty);
            }
            else {
                $scope.empty = false;
            }

            $scope.loadMore = function () {
                $scope.isLoading = true;
                $log.debug('isLoading',$scope.isLoading);
                $scope.link = data.next;
                $log.debug('loadmore()', $scope.link);
                var next = $scope.link.split('&')[2];
                $log.debug('split ', next);
                homeService.newsInfo({ next: next }, function (response) {
                    $scope.isLoading = false;
                    $log.debug('response ', response);
                    $scope.newsInfo = $scope.newsInfo.concat(response.posts);
                });
            }
        }
    });

    angular.element($window).bind("scroll", function () {
        var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        var body = document.body, html = document.documentElement;
        var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        windowBottom = windowHeight + window.pageYOffset;
        if (windowBottom >= docHeight) {
            $log.debug('bottom reached');
            $scope.loadMore();
        }
    });

    $scope.date = function (date) {
        if (!date) {
            return ' ';
        }
        if (date) {
            var d = new Date(date);
            var formattedDate = d.getDate() + "-" + d.getMonth() + "-" + d.getFullYear();
            var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
            var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
            var formattedTime = hours + ":" + minutes;
            var fDate = formattedTime + " " + formattedDate;
            return fDate;
        }
    }
};