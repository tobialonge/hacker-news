angular.module('hackerNews')
    .factory('homeService', ['$resource', '$log', '$rootScope', '$state', function ($resource, $log, $rootScope, $http, $state) {
        var base = 'https://webhose.io/';
        var homeResource = $resource(base, {}, {
            newsInfo: {
                method: 'GET',
                url: base + '/search?token=9b72a5e2-fda1-419d-99b3-ba0ee6b66c8e&size=30&format=json&q=language%3A(english)%20site_category%3Atech%20(site_type%3Anews%20OR%20site_type%3Ablogs)&:next',
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }
        }); return homeResource;
    }]);